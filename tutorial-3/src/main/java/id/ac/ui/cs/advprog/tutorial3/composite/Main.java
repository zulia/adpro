package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;

public class Main {
    public static void main(String[] args) {
        Employees employees = new BackendProgrammer("Khatya", 100000);
        System.out.println(employees.getName() + " adalah " + employees.getRole()
                + " pemalas makanya gajinya kecil, sebesar " + employees.getSalary());
    }
}
