package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class BackendProgrammer extends Employees {
    //TODO Implement
    public BackendProgrammer(String name, double salary) {
        this.name = name;
        this.salary = setSalary(salary);
        this.role = "Back End Programmer";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }


    @Override
    public double setSalary(double salary) {
        if (salary < 30000) {
            throw new IllegalArgumentException();
        } else {
            return salary;
        }
    }
}
