package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.NoCrustSandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChiliSauce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cucumber;


public class Main {
    public static void main(String[] args) {
        CrustySandwich cs = new CrustySandwich();
        BeefMeat bm = new BeefMeat(cs);

        System.out.println("You bought " + bm.getDescription() + " and you must pay " + bm.cost());

        NoCrustSandwich ncs = new NoCrustSandwich();
        ChiliSauce chili = new ChiliSauce(ncs);
        ChickenMeat chickenMeat = new ChickenMeat(chili);
        Cucumber cucumber = new Cucumber(chickenMeat);

        System.out.println("You bought " + cucumber.getDescription()
                + " and you must pay " + cucumber.cost());
    }
}
