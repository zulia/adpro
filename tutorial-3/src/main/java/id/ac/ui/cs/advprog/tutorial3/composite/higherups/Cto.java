package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public Cto(String name, double salary) {
        this.name = name;
        this.salary = setSalary(salary);
        this.role = "CTO";
    }


    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }

    @Override
    public double setSalary(double salary) {
        if (salary < 100000) {
            throw new IllegalArgumentException();
        } else {
            return salary;
        }
    }
}
