package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) {
        this.name = name;
        this.salary = setSalary(salary);
        this.role = "CEO";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }

    public double setSalary(double salary) {
        if (salary < 200000) {
            throw new IllegalArgumentException();
        } else {
            return salary;
        }
    }
}
