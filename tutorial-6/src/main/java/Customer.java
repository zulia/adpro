import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        double totalAmount = 0;
        int frequentRenterPoints = 0;

        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";
        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Add frequent renter points
            frequentRenterPoints++;

            // Add bonus for a two day new release rental
            frequentRenterPoints += each.getFrequentRenterPoints();

            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t"
                    + String.valueOf(each.getCharge()) + "\n";
            totalAmount += each.getCharge();
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(getTotalCharge())
                + "\n";
        result += "You earned " + String.valueOf(getTotalFrequentRenterPoints())
                + " frequent renter points";

        return result;
    }

    private double getTotalCharge() {
        double result = 0;
        Iterator<Rental> rental = rentals.iterator();
        while (rental.hasNext()) {
            Rental each = rental.next();
            result += each.getCharge();
        }
        return result;
    }

    private int getTotalFrequentRenterPoints() {
        int result = 0;
        Iterator<Rental> rental  = rentals.iterator();
        while (rental.hasNext()) {
            Rental each = rental.next();
            result += each.getFrequentRenterPoints();
        }
        return result;
    }

    public String htmlStatement() {
        Iterator<Rental> rental = rentals.iterator();
        String result = "<H1>Rentals for <EM>" + getName() + "</EM></H1><P>\n";
        while (rental.hasNext()) {
            Rental each = rental.next();
            result += each.getMovie().getTitle() + ": "
                    + String.valueOf(each.getCharge()) + "<BR>\n";
        }
        result += "<P>You owe<EM>" + String.valueOf(getTotalCharge() + "</EM><P>\n");
        result += "On this rental you earned <EM>"
                + String.valueOf(getTotalFrequentRenterPoints())
                + "</EM> frequent renter points<P>";

        return result;
    }
}