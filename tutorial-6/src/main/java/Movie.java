import java.util.Objects;

public class Movie {

    public static final int CHILDREN = 2;
    public static final int REGULAR = 0;
    public static final int NEW_RELEASE = 1;

    private String title;
    private Price priceCodes;
    private int priceCode;

    public Movie(String title, int priceCode) {
        this.title = title;
        setPriceCode(priceCode);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPriceCode() {
        return priceCodes.getPriceCode();
    }

    public void setPriceCode(int arg) {
        switch (arg) {
            case Movie.REGULAR:
                priceCodes = new RegularPrice();
                break;
            case Movie.NEW_RELEASE:
                priceCodes = new NewReleasePrice();
                break;
            case Movie.CHILDREN:
                priceCodes = new ChildrensPrice();
                break;
            default:
                throw new IllegalArgumentException("Incorrect Type Code Value");
        }
    }

    public double getCharge(int daysRented) {
        return priceCodes.getCharge(daysRented);
    }


    int getFrequentRenterPoints(int daysRented) {
       return priceCodes.getFrequentRenterPoints(daysRented);
    }
}