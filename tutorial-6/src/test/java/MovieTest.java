import org.junit.Test;

import static org.junit.Assert.*;

public class MovieTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    @Test
    public void getTitle() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        assertEquals("Who Killed Captain Alex?", movie.getTitle());

        movie.setTitle("Bad Black");

        assertEquals("Bad Black", movie.getTitle());

        assertEquals(Movie.REGULAR, movie.getPriceCode());

        movie.setPriceCode(Movie.CHILDREN);

        assertEquals(Movie.CHILDREN, movie.getPriceCode());

        Movie movie1 = new Movie("Spongebob", Movie.CHILDREN);
        assertEquals("Spongebob", movie1.getTitle());
    }

    @Test(expected = IllegalArgumentException.class)
    public void movieError() {
        Movie movie = new Movie("Duck a Duck", 10);

    }
}