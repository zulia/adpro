import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    @Test
    public void statementWithSingleMovie() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        Rental rent = new Rental(movie, 3);
        Customer customer = new Customer("Alice");
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");


        assertEquals("Alice", customer.getName());
        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        Movie movie2 = new Movie("Zulia The Potato", Movie.NEW_RELEASE);
        Movie movie3 = new Movie("WOHOHOHO", Movie.CHILDREN);
        Movie movie4 = new Movie("WAKAKKAAK", Movie.CHILDREN);
        Movie movie5 = new Movie("HAHAHHA", Movie.NEW_RELEASE);
        Rental rent = new Rental(movie, 3);
        Rental rent2 = new Rental(movie2, 4);
        Rental rent3 = new Rental(movie3, 4);
        Rental rent4 = new Rental(movie4, 1);
        Rental rent5 = new Rental(movie5, 1);
        Customer customer = new Customer("Alice");
        customer.addRental(rent);
        customer.addRental(rent2);
        customer.addRental(rent3);
        customer.addRental(rent4);
        customer.addRental(rent5);



        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(8, lines.length);
        assertTrue(result.contains("Amount owed is 23.0"));
        assertTrue(result.contains("6 frequent renter points"));

    }

    @Test
    public void htmlStatements(){
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        Rental rent = new Rental(movie, 3);
        Customer customer = new Customer("Alice");
        customer.addRental(rent);

        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        for (String s : lines){
            System.out.println(s);
        }

        assertTrue(result.contains("owe<EM>3.5</EM><P>"));
        assertTrue(result.contains("<EM>1</EM> frequent renter points"));
    }
}