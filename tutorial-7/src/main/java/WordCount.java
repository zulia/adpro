import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 2nd exercise.
 */
public class WordCount {
    public static long countLines(String word, Path filePath) {
        long count = 0;
        try {
            count = Files.lines(filePath)
                    .filter(line -> line.contains(word))
                    .count();
            System.out.println(String.format("The word substring '%s' occurred in %d lines",
                    word, count));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }
}

