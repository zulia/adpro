package applicant;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ApplicantTest {
    // TODO Implement me!
    // Increase code coverage in Applicant class
    // by creating unit test(s)!

    private static final boolean CREDIBILITY = true;
    private static final int CREDIT_SCORE = 800;
    private static final int YEARS_OF_EMPLOYEE= 10;
    private static final boolean CRIMINAL_RECORD = true;
    Applicant applicant;

    @Before
    public void setUp() {
        applicant = new Applicant();
    }



    @Test
    public void testIsCredible() {
        Predicate<Applicant> qualifiedEvaluator = Applicant::isCredible;
        Predicate<Applicant> creditEvaluator = anApplicant -> anApplicant.getCreditScore() > 600;
        Predicate<Applicant> employmentEvaluator = anApplicant1 -> anApplicant1.getEmploymentYears() > 0;
        Predicate<Applicant> crimeCheck = applicant1 -> !applicant1.hasCriminalRecord();

        assertTrue(Applicant.evaluate(applicant, qualifiedEvaluator));
        assertTrue(Applicant.evaluate(applicant, qualifiedEvaluator.and(creditEvaluator)));
        assertTrue(Applicant.evaluate(applicant, qualifiedEvaluator.and(creditEvaluator).and(employmentEvaluator)));
        assertFalse(Applicant.evaluate(applicant, qualifiedEvaluator.and(creditEvaluator).and(employmentEvaluator).and(crimeCheck)));
    }

    @Test
    public void testThePrinting() {
        Predicate<Applicant> qualifiedEvaluator = Applicant::isCredible;

        assertEquals("Result of evaluating applicant: accepted", applicant.printEvaluation(Applicant.evaluate(applicant, qualifiedEvaluator)));

    }


}
