import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
public class ScoreGroupingTest {
    // TODO Implement me!
    // Increase code coverage in ScoreGrouping class
    // by creating unit test(s)!

    Map<String, Integer> scores = new HashMap<>();

    @Before
    public void setUp() {
        scores.put("Zulia", 3);
        scores.put("Potato", 3);
    }
    @Test
    public void TestPartiCipation() {
        assertEquals(1, ScoreGrouping.groupByScores(scores).size());
    }
}