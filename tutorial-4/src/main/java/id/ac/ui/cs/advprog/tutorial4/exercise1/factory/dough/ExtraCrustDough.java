package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class ExtraCrustDough implements Dough {
    public String toString() {
        return "Extra Crust Dough";
    }
}
